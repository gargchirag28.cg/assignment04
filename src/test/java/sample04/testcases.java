package sample04;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;


public class testcases 
{
	
	
	  void postreq(traveler t,String f,String s,int i,int j) throws IOException
	  {
		
		RestAssured.baseURI="https://info.payu.in/merchant/postservice.php/?form=2";
	    Response response=RestAssured.given().urlEncodingEnabled(true)
		.param("form",t.form)
		.param("key", t.key)
		.param("command",t.command)
		.param("hash",t.hash)
		.param("var1",t.var1)
		.param("var2",t.var2)
		.param("var3",t.var3)
		.param("var4",t.var4)
		.param("var5",t.var5)
		.param("var6",t.var6)
		.param("var7",t.var7)
		.param("var8",t.var8)
		.post()
		.then()
		//.statusCode(200)
		.extract()
		.response();
		System.out.println(response.asString());
		Assert.assertEquals(response.getStatusCode(), 200);
	  //	String token=response.jsonPath().get("cardToken");
	  //	System.out.println("cardToken is  = "+ token);
		//XLUtils.setcelldata(f,s,i,j,token);
	  }
	@Test
	   void delreq(traveler t)
	   {
		   
		   String token=getreq(t);
		   
		   RestAssured.baseURI=t.base;
		//RestAssured.baseURI="https://info.payu.in/merchant/postservice.php/?form=2";
		
		   Response response=RestAssured.given().urlEncodingEnabled(true)
				.param("form",t.form)   
				.param("key",t.key)
				.param("command",t.command)
				.param("hash",t.hash)
				.param("var1", t.var1)
				.param("var2",token) 
				    .post()
				.then()
				.statusCode(200)
				.extract()
				.response();
				System.out.println("response is"+response.asString());
				Assert.assertEquals(response.getStatusCode(), 200);
		  
	   }
	   
	   String getreq(traveler t)
	   {
		   RestAssured.baseURI=t.base;
		   Response response=RestAssured.given().urlEncodingEnabled(true)
				   .param("form",t.form)
				   .param("key",t.key)
				   .param("command",t.command)
				   .param("hash", t.hash)
				   .param("var1", t.var1)
				   .post()
				   .then()
				   .statusCode(200)
				.extract()
				.response();
		        
				System.out.println(response.asString()); 
				
				JsonPath json = response.jsonPath();
				String token;
				String msg = json.get("msg");

				if (msg.equals("Card not found.")) {
					token = "No token";
				} else 
				{
					Map<String, Map<String, String>> val = new LinkedHashMap<String, Map<String, String>>();
					val = json.get("user_cards");
					Map.Entry<String, Map<String, String>> entry = val.entrySet().iterator().next();
					token = entry.getKey();
				}
            return token;
		   
	   }
	   
	
}
