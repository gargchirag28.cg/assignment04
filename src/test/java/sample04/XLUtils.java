package sample04;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class XLUtils
{
	public static FileInputStream fi;
	   public static FileOutputStream fo;
	   public static XSSFWorkbook wb;
	   public static XSSFSheet ws;
	   public static XSSFRow row;
	   public static XSSFCell cell;
	   
	   public static int getRowCount(String xlfile,String xlsheet) throws IOException
	   {
		   fi=new FileInputStream(xlfile);
		   wb=new XSSFWorkbook(fi);
		   ws=wb.getSheet(xlsheet);
		   int rowcount=ws.getLastRowNum();
		  // wb.close();
		   fi.close();
		   return rowcount;
	   }
	    
	   public static int getcellcount(String xlfile,String xlsheet,int rownum) throws IOException
	   {
		   fi=new FileInputStream(xlfile);
		   wb=new XSSFWorkbook(fi);
		   ws=wb.getSheet(xlsheet);
		   row=ws.getRow(rownum);
		   int cellcount=row.getLastCellNum();
		   fi.close();
		   return cellcount;
	   }
	   
	   public static String getcelldata(String xlfile,String xlsheet,int rownum,int colnum) throws IOException
	   {
		   fi=new FileInputStream(xlfile);
		   wb=new XSSFWorkbook(fi);
		   ws=wb.getSheet(xlsheet);
		   row=ws.getRow(rownum);
		   cell=row.getCell(colnum);
		   String data;
		   try 
		   {
			DataFormatter formatter =new DataFormatter();
			String celldata=formatter.formatCellValue(cell);
			return celldata;
		   }
		   catch(Exception e)
		   {
			   data=" ";
		   }
		 //  wb.close();
		   fi.close();
		   return data;
	   }
	   
	   public static void setcelldata (String xlfile,String xlsheet,int rownum,int colnum,String data) throws IOException
	   {
		   fi=new FileInputStream(xlfile);
		   wb=new XSSFWorkbook(fi);
		   ws=wb.getSheet(xlsheet);
		   row=ws.getRow(rownum);
		   cell=row.createCell(colnum);
		   cell.setCellValue(data);
		   fo=new FileOutputStream(xlfile);
		   wb.write(fo);
		   fi.close();
		   fo.close();
	   }

	   public static String hash(String input)
	   {
		   try { 
	            // getInstance() method is called with algorithm SHA-512 
	            MessageDigest md = MessageDigest.getInstance("SHA-512"); 
	  
	            // digest() method is called 
	            // to calculate message digest of the input string 
	            // returned as array of byte 
	            byte[] messageDigest = md.digest(input.getBytes()); 
	  
	            // Convert byte array into signum representation 
	            BigInteger no = new BigInteger(1, messageDigest); 
	  
	            // Convert message digest into hex value 
	            String hashtext = no.toString(16); 
	  
	            // Add preceding 0s to make it 32 bit 
	            while (hashtext.length() < 128) { 
	                hashtext = "0" + hashtext; 
	            } 
	  
	            // return the HashText 
	            return hashtext; 
	        } 
	  
	        // For specifying wrong message digest algorithms 
	        catch (NoSuchAlgorithmException e) { 
	            throw new RuntimeException(e); 
	        } 
	   }
	   
}
